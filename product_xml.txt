 <StackPanel>
            <Grid Height="50">
                <Image Source="/Resources/logo.png" Width="50" HorizontalAlignment="Left"/>
                <Label Content="Флорист приветствует вас!" HorizontalAlignment="Center"/>
                <Label HorizontalAlignment="Right" x:Name="lblRole"/>
            </Grid>

            <StackPanel Orientation="Horizontal">
                <Label Content="Поиск"/>
                <TextBox x:Name="txtSearch" Width="200" TextChanged="TxtSearch_TextChanged"/>
                <Label Content="Сортировка"/>
                <ComboBox Name="cmbSort" Width="200" SelectionChanged="CmbSort_SelectionChanged"/>
                <Label Content="Фильтрация"/>
                <ComboBox Name="cmbFilt" Width="200" SelectionChanged="CmbFilt_SelectionChanged"/>
            </StackPanel>

            <Label x:Name="lblCount"/>


            <ListView x:Name="lvProducts" Height="250">
                <ListView.ItemTemplate>
                    <DataTemplate>
                        <StackPanel Orientation="Horizontal" Background="{Binding getBack}">
                            <Image Source="{Binding ProductPhotoBitmap, TargetNullValue={StaticResource picture}}" Width="50"/>

                            <StackPanel Orientation="Vertical" Width="400">
                                <Label Content="{Binding ProductName}"/>
                                <Label Content="{Binding ProductDescription}"/>
                                <Label Content="{Binding ProductManufacturer}"/>
                                <TextBlock Text="{Binding ProductCost}" TextDecorations="{Binding getTextDecor}"/>
                                <TextBlock Text="{Binding getSale}"/>
                            </StackPanel>

                            <Label Content="{Binding ProductDiscountAmount}" />

                        </StackPanel>
                    </DataTemplate>
                </ListView.ItemTemplate>
            </ListView>

            <Button x:Name="btnExit" Click="BtnExit_Click" Content="Выход" HorizontalAlignment="Right"/>
        </StackPanel>